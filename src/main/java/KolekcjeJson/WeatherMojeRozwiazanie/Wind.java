package KolekcjeJson.WeatherMojeRozwiazanie;

public class Wind {

    private Float speed;
    private Short deg;

    public Float getSpeed() {
        return speed;
    }

    public void setSpeed(Float speed) {
        this.speed = speed;
    }

    public Short getDeg() {
        return deg;
    }

    public void setDeg(Short deg) {
        this.deg = deg;
    }

    @Override
    public String toString() {
        return "Wind{" +
                "speed=" + speed +
                ", deg=" + deg +
                '}';
    }
}
