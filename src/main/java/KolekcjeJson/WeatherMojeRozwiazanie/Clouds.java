package KolekcjeJson.WeatherMojeRozwiazanie;

public class Clouds {
    private Short all;

    public Short getAll() {
        return all;
    }

    public void setAll(Short all) {
        this.all = all;
    }

    @Override
    public String toString() {
        return "Clouds{" +
                "all=" + all +
                '}';
    }
}
