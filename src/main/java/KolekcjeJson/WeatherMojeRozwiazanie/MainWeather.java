package KolekcjeJson.WeatherMojeRozwiazanie;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainWeather {
    public static void main(String[] args) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        Pola pola = objectMapper.readValue(new File("Weather.json"), Pola.class);
        System.out.println("Id: " + pola.getId());
        System.out.println("Imię: " + pola.getName());
        System.out.println("Cod: " + pola.getCod());

        Coord coord = pola.getCoord();
        System.out.println("Lon: " + coord.getLon());
        System.out.println("Lat: " + coord.getLat());

        List<Weather> weathers = pola.getWeather();
        for (Weather element : weathers) {
            System.out.println("Id weather: " + element.getId());
            System.out.println("Main nazwa: " + element.getMain());
            System.out.println("Description: " + element.getDescription());
            System.out.println("Icon: " + element.getIcon());
        }

        System.out.println("Base pole: " + pola.getBase());

        Main2a main2a = pola.getMain2a();
        System.out.println("Temeperatura: " + main2a.getTemp());
        System.out.println("Cieśnienie: " + main2a.getPressure());
        System.out.println("Wilgotność: " + main2a.getHumidity());
        System.out.println("temperatura minimalna: " + main2a.getTempMin());
        System.out.println("temperatura maxymalna" + main2a.getTempMax());

        System.out.println("Widoczność: " + pola.getVisibility());

        Wind wind = pola.getWind();
        System.out.println("prędkość: " + wind.getSpeed());
        System.out.println("deg: " + wind.getDeg());

        Clouds clouds = pola.getClouds();
        System.out.println("Wszystko: " + clouds.getAll());

        System.out.println("Dt: " + pola.getDt());

        Sys sys = pola.getSys();
        System.out.println("Typ: " + sys.getType());
        System.out.println("Numer Id: " + sys.getId());
        System.out.println("Wiadomość: " + sys.getMessage());
        System.out.println("Kraj: " + sys.getCountry());
        System.out.println("Wschód słońca: " + sys.getSunrise());
        System.out.println("Zachód słońca: " + sys.getSunset());


    }
}

