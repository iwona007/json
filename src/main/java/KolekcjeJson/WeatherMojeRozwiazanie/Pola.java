package KolekcjeJson.WeatherMojeRozwiazanie;

import java.util.ArrayList;
import java.util.List;

public class Pola {

    private Integer id; //pole
    private String name; //pole
    private Short cod; //pole
    private Coord coord; //klasa
    private List<Weather> weather = new ArrayList<>();//lista
    private String base; //pole
    private Main2a main2a; //klasa
    private Short visibility; //pole
    private Wind wind; //klasa
    private Clouds clouds; //klasa
    private Integer dt; //pole
    private Sys sys; //klasa


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getCod() {
        return cod;
    }

    public void setCod(Short cod) {
        this.cod = cod;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Main2a getMain2a() {
        return main2a;
    }

    public void setMain2a(Main2a main2a) {
        this.main2a = main2a;
    }

    public Short getVisibility() {
        return visibility;
    }

    public void setVisibility(Short visibility) {
        this.visibility = visibility;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    @Override
    public String toString() {
        return "Pola{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", coord=" + coord +
                ", weather=" + weather +
                ", base='" + base + '\'' +
                ", main2a=" + main2a +
                ", visibility=" + visibility +
                ", wind=" + wind +
                ", clouds=" + clouds +
                ", dt=" + dt +
                ", sys=" + sys +
                '}';
    }
}

