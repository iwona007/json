package KolekcjeJson.WeatherMojeRozwiazanie;

public class Coord {
    private Short lon;
    private Short lat;

    public Short getLon() {
        return lon;
    }

    public void setLon(Short lon) {
        this.lon = lon;
    }

    public Short getLat() {
        return lat;
    }

    public void setLat(Short lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "Coord{" +
                "lon=" + lon +
                ", lat=" + lat +
                '}';
    }
}
